package Massive;

//Посчитать сумму элементов массива с нечетными индексами
public class task5 {
    public static void main(String[] args) {
        System.out.println(sum(new int[]{1, 2, 3, -4, 5, 6, 7}));
    }
    public static int sum(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        int sum = 0;
        for (int i = 0; i < list.length; i++) {
            if (i % 2 == 0) {
                sum += list[i];
            }
        }
        return sum;
    }
}
