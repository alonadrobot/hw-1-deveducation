package Massive;

//	Поменять местами первую и вторую половину массива, например, для массива
//1 2 3 4, результат 3 4 1 2
public class task8 {
    public static void main(String[] args) {
        System.out.println(revers(new int[]{1, 2, 3,4}));
    }
    public static String revers(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        String rev = "";
        if (list.length % 2 == 0) {
            for (int i = 0; i < list.length / 2; i++) {
                int temp = list[i];
                list[i] = list[list.length / 2 + i];
                list[list.length / 2 + i] = temp;
            }
        } else {
            for (int i = 0; i < list.length / 2; i++) {
                int temp = list[i];
                list[i] = list[list.length / 2 + 1 + i];
                list[list.length / 2 + 1 + i] = temp;
            }
        }
        for (int i = 0; i < list.length; i++) {
            rev = rev + list[i];
        }
        return rev;
    }
}
