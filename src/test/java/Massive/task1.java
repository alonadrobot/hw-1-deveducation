package Massive;

//Найти минимальный элемент массива
public class task1 {
    public static void main(String[] args) {
        System.out.println(min(new int[]{1, 2, 3, -4, 5, 6, 7}));
    }
    public static int min(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.print("\n");
        int min = list[0];
        for (int i = 0; i < list.length; i++) {
            if (list[i] < min) {
                min = list[i];
            }
        }
        return min;
    }
}
