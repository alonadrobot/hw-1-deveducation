package Massive;

//	Найти индекс максимального элемента массива
public class task4 {
    public static void main(String[] args) {
        System.out.println(maxIndex(new int[]{1, 2, 3, -4, 5, 6, 7}));

    }
    public static int maxIndex(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        int index = 0;
        int max = list[0];
        for (int i = 0; i < list.length; i++) {
            if (list[i] > max) {
                max = list[i];
                index = i;
            }
        }
        return index;
    }
}
