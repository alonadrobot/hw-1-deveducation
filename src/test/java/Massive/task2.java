package Massive;

//Найти максимальный элемент массива
public class task2 {
    public static void main(String[] args) {
        System.out.println(max(new int[]{1, 2, 3, -4, 5, 6, 7}));
    }

    public static int max(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        int max = list[0];
        for (int i = 0; i < list.length; i++) {
            if (list[i] > max) {
                max = list[i];
            }
        }
        return max;
    }
}
