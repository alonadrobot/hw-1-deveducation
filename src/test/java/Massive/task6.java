package Massive;

//	Сделать реверс массива (массив в обратном направлении)
public class task6 {
    public static void main(String[] args) {

        System.out.println(revers(new int[]{1, 2, 3, -4, 5, 6, 7}));
    }
    public static String revers(int[] list) {

        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        String rev = "";
        for (int i = 0; i < list.length / 2; i++) {
            int temp = list[i];
            list[i] = list[list.length - 1 - i];
            list[list.length - 1 - i] = temp;
        }
        for (int i = 0; i < list.length; i++) {
            rev = rev + list[i];
        }
        return rev;
    }
}
