package Function;
//Найти расстояние между двумя точками в двухмерном декартовом пространстве
public class task2 {
    public static void main(String[] args) {
        System.out.println(show(5,-3,4,8));
    }
    public static int show ( int x1, int y1, int x2, int y2){
        int res = (int)Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        return res;
    }

    }