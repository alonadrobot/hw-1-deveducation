package Operation;
//Найти суммы только положительных из трех чисел
public class task3 {
    public static void main(String[] args) {
        System.out.println(show(4,-3,6));
    }
    public static int show(int number1, int number2, int number3){
        int result=0;
        int[]array={number1,number2,number3};
        for(int i=0;i<array.length;i++){
            if(array[i]>0){
                result=result+array[i];
            }
        }
        return result;
    }
}
