package Cycle;

import org.junit.Assert;
import org.junit.Test;

public class task6Test {
    @Test
    public void test(){
        task6 task6=new task6();
        String res=task6.show("1234");
        Assert.assertEquals("Зеркальное отображение: "+4321,res);
        String res1=task6.show("734344");
        Assert.assertEquals("Зеркальное отображение: "+443437,res1);
    }

}