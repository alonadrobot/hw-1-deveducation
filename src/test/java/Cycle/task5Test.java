package Cycle;

import org.junit.Assert;
import org.junit.Test;

public class task5Test {
    @Test
    public void test(){
        task5 task5=new task5();
        String res=task5.show(123);
        Assert.assertEquals("Сумма цифр заданого числа: "+6,res);
        String res1=task5.show(5072);
        Assert.assertEquals("Сумма цифр заданого числа: "+14,res1);
        String res2=task5.show(-2-30);
        Assert.assertEquals("Сумма цифр заданого числа: "+(-5),res2);
    }

}