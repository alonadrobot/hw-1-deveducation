package Cycle;
//Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)
public class task2 {
    public static void main(String[] args) {
        System.out.println(show(3));
    }
    public static String show(int num){
        int temp;
        boolean isPrime = true;
        for (int i = 2; i <= num - 1; i++) {
            temp = num % i;
            if (temp == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            return (num+" простое число");
        } else {
            return (num+" составное число");
        }
    }
}
