package Cycle;
// Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.
public class task6 {
    public static void main(String[] args) {
        System.out.println(show("1234"));
    }
    public static String show(String num){
        String rev="";
        for(int i=num.length()-1; i>=0; i--){
            rev=rev+num.charAt(i);
        }
        return ("Зеркальное отображение: "+rev);

    }
}
