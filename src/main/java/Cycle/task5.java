package Cycle;
//	Посчитать сумму цифр заданного числа
public class task5 {
    public static void main(String[] args) {
        System.out.println(show(123));
    }
    public static String show(int num){
        int sum=0;
        while(num != 0){
            sum =sum + (num % 10);
            num=num/10;
            System.out.println(num);
        }
        return ("Сумма цифр заданого числа: "+sum);
    }
}
