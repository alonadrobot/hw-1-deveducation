package Cycle;
//	Вычислить факториал числа n. n! = 1*2*…*n-1*n;
public class task4 {
    public static void main(String[] args) {
        System.out.println(show(10,0));
    }
    public static int show(int num1, int num2){
        int result = 1;
        for (int i = 1; i <= num1; i++) {
            result = result * i;
            num2=result;
        }
        return num2;
    }
}
