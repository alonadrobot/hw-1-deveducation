package Operation;
//Если а – четное посчитать а*б, иначе а+б
public class task1 {
    public static void main(String[] args) {
        System.out.println(show(3, 4));
    }
    public static int show(int a, int b) {
        int result;
            if (a % 2 == 0) {
                result = a * b;
            } else {
                result = a + b;
            }
            return result;
    }
}
