package Operation;
//Написать программу определения оценки студента по его рейтингу, на основе следующих правил
public class task5 {
    public static void main(String[] args) {
        System.out.println(show(100));
    }
    public static String show(int grade){
        if(grade<0){
            return ("Введите число больше 0");
        }
        if(grade>100){
            return ("Введите число меньше 100 или равное ему");
        }
        if(grade>=0&&grade<=19) {
            return ("Ваша оценка F");
        }
        if(grade>=20&&grade<=39) {
            return ("Ваша оценка E");
        }
        if(grade>=40&&grade<=59) {
            return ("Ваша оценка D");
        }
        if(grade>=60&&grade<=74) {
            return ("Ваша оценка C");
        }
        if(grade>=75&&grade<=89) {
            return ("Ваша оценка B");
        }
        if(grade>=90&&grade<=100) {
            return ("Ваша оценка A");
        }
        return null;
    }
}
