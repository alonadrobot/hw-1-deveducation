package Operation;
//Определить какой четверти принадлежит точка с координатами (х,у)
public class task2 {
    public static void main(String[] args) {
        System.out.println(show(3,-4));
        }
        public static String show(int x, int y) {
            if (x > 0 && y > 0) {
                return ("В правом верхнем углу");
            }
            if (x < 0 && y < 0) {
                return ("В левом нижнем углу");
            }
            if (x < 0 && y > 0) {
                return ("В левом верхнем углу");
            }
            if (x > 0 && y < 0) {
                return ("В правом нижнем углу");
            }
            if (x == 0 && y == 0) {
                return ("Центр координат");
            }
            if (x == 0) {
                return ("Точка находится по оси х");
            }
            if (y == 0) {
                return ("Точка находится по оси у");
            }
            return null;
        }
}
