package Function;

//
public class task3 {

    public static void main(String[] args) {
        System.out.println(show(19));
    }

        public static String show(int number) {
            String[] BELOW_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
            String[] TENS = {"сто", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
            String[] HUNDREDS = {"тысяча", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
            String showValue;

            if (number % 100 < 20 && number != 100){
                showValue = BELOW_TWENTY[number % 100];
                number /= 100;
            }
            else {
                if (number == 100) {
                    return (TENS[0]);
                }
                showValue = BELOW_TWENTY[number % 10];
                number /= 10;

                showValue = TENS[number % 10] + " " + showValue;
                number /= 10;
            }
            if (number == 0) return showValue;
            return HUNDREDS[number] + " " + showValue;
        }
    }