package Massive;

//	Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
public class task9 {
    public static void main(String[] args) {
        System.out.println(array1(new int[]{6, 8, 3, 2}));
        System.out.println(array2(new int[]{6, 8, 3, 7}));
        System.out.println(array3(new int[]{8, 3, 7, 6}));
    }

    public static String array1(int[] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 1 + i; j < array.length; j++) {
                int temp = array[i];
                if (temp > array[j]) {
                    int x = array[j];
                    array[i] = x;
                    array[j] = temp;
                }
            }
        }
        String rev = "";
        for (int i = 0; i < array.length; i++) {
            rev = rev + array[i];
        }
        return rev;
    }

    public static String array2(int[] array){
        for (int i = 0; i < array.length; i++) {
            int pos = i;
            int min = array[i];
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    pos = j;
                    min = array[j];
                }
            }
            array[pos] = array[i];
            array[i] = min;
        }
        String rev = "";
        for (int i = 0; i < array.length; i++) {
            rev = rev + array[i];
        }
        return rev;
    }

    public static String array3(int[] array){
        for (int i=0;i<array.length;i++){
            int pos=array[i];
            int j=i-1;
            for(;j>=0;j--){
                if(pos<array[j]){
                    array[j+1]=array[j];
                }else break;
            }
            array[j+1]=pos;
        }
        String rev = "";
        for (int i = 0; i < array.length; i++) {
            rev = rev + array[i];
        }
        return rev;
    }
}
