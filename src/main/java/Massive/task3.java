package Massive;

//Найти индекс минимального элемента массива
public class task3 {
        public static void main (String[]args){
            System.out.println((new int[]{1, 2, 3, -4, 5, 6, 7}));
        }
        public static int minIndex ( int[] list){

            for (int i = 0; i < list.length; i++) {

                System.out.print(list[i]);
            }
            System.out.println("\n");
            int index = 0;
            int min = list[0];
            for (int i = 0; i < list.length; i++) {
                if (list[i] < min) {
                    min = list[i];
                    index = i;
                }

            }
            return index;
        }
    }